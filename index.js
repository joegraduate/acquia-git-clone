'use strict'

const Hapi = require('hapi')
const fetch = require('node-fetch')
const axios = require('axios')
const settings = require('./settings.json')
const simpleGit = require('simple-git')

const server = new Hapi.Server()
server.connection({ port: 3000 })

server.route({
  method: 'POST',
  path: '/bb',
  handler: (request, reply) => {
    if (request.payload.push.changes[0].commits[0].hash.substring(0, 7)) {
      console.log(`Time to send a message ${request.payload.push.changes[0].commits[0].hash.substring(0, 7)}`)
      var commit = {
        'message': request.payload.push.changes[0].commits[0].message,
        'link': request.payload.push.changes[0].commits[0].links.html.href,
        'hash': request.payload.push.changes[0].commits[0].hash.substring(0, 7),
        'author': request.payload.push.changes[0].commits[0].author.raw,
        'repo': request.payload.repository.name,
        'branch': request.payload.push.changes[0].new.name
      }
      var message = {
        'channel': '#acquia-code-updates',
        'username': 'hexo',
        'attachments': [{
          'fallback': `[${commit.repo}/${commit.branch}] has been updated.`,
          'pretext': `[${commit.repo}/${commit.branch}]`,
          'color': '#6CDDB5',
          'text': `\`<${commit.link}|${commit.hash}>\` ${commit.message.replace(/\r?\n|\r/g, ' ')} \n _commit by *${commit.author}*_`,
          'mrkdwn_in': [
            'text',
            'pretext'
          ]
        }],
        'icon_emoji': ':beryl:'
      }

      fetch('https://hooks.slack.com/services/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', { method: 'POST', body: JSON.stringify(message) })
      reply('Request Received')
    }
  }
})

server.route({
  method: 'POST',
  path: '/update/{repo}',
  handler: function (request, reply) {
    console.log(`Updating ${request.params.repo}`)
    let repo = require('simple-git')(`./repo/${request.params.repo}`)
    repo.fetch().then(() => {
      repo.push()
    })

    reply('Request Received')
  }
})

let authOptions = {
  url: 'https://cloudapi.acquia.com/v1/sites.json',
  auth: {
    username: settings.acquiaCloud.email,
    password: settings.acquiaCloud.key
  }
}
function cloneRepos (sites) {
  for (let site of sites) {
    console.log(site)
    axios({
      url: `https://cloudapi.acquia.com/v1/sites/${site}.json`,
      auth: authOptions['auth']
    }).then(res => {
      simpleGit().mirror(res.data.vcs_url, `./repo/${res.data.unix_username}`)
    })
  }
}

server.route({
  method: 'GET',
  path: '/init',
  handler: (request, reply) => axios(authOptions).then(res => {
    return res.data
  }).then(sites => {
    cloneRepos(sites)
  })
})

server.route({
  method: 'GET',
  path: '/',
  handler: (request, reply) => reply('Hello, world!')
})

server.start(() => console.log(`Server running at:${server.info.uri}`))
